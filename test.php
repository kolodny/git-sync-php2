#!/usr/bin/env php
<?php

error_reporting(E_ERROR | E_PARSE);

$cwd = getcwd();
$passed = 0;
$failed = 0;
$arg = $argv[1];
$start = $argv[2];
$i = 0;
foreach (get_tests() as $test_name => $test) {
	if ($start && $start != $i++) continue;
	if (!$arg || $arg == 'all' || $arg == $test_name) {
		if (!$test()) {
			echo "$test_name failed\n";
			$failed++;
		} else {
			$passed++;
			echo "\n";
		}
	}
}
echo "passed: $passed, failed: $failed\n";



function get_tests() {
	return array(
		'setup' => function() {
			global $cwd;
			setup();
			chdir($cwd);
			if (!file_exists('repos/origin/.sync')) return false;
			if (!file_exists('repos/local/.sync')) return false;
			chdir('repos/origin');
			if (exec('git --git-dir=.sync/ status --porcelain') != '') return false;
			chdir('../local');
			if (exec('git --git-dir=.sync/ status --porcelain') != '') return false;
			return true;
		},
		'sync_up' => function() {
			setup();
			sync_up($output);
			echo implode("\n", $output) . "\n";
			chdir('origin');
			return file_get_contents('test.txt') == "from local\n";
		},
		'sync_down' => function() {
			setup();
			sync_down($output);
			echo implode("\n", $output) . "\n";
			chdir('local');
			return file_get_contents('test.txt') == "from origin\n";			
		},
		'sync_up_then_down' => function() {
			setup();
			sync_up($output);
			sync_down($output);
			echo implode("\n", $output) . "\n";
			chdir('local');
			return file_get_contents('test.txt') == "from local\nfrom origin\n";
		},
		'sync_down_then_up' => function() {
			setup();
			sync_down($output);
			sync_up($output);
			echo implode("\n", $output) . "\n";
			chdir('local');
			return file_get_contents('test.txt') == "from origin\nfrom local\n";
		},
		'sync_up_when_no_changes' => function() {
			setup();
			sync_up($output);
			sync_up($output, true);
			$output = implode("\n", $output);
			echo $output . "\n";
			return strpos($output, 'no changes detected') !== false;	
		},
		'sync_down_when_no_changes' => function() {
			setup();
			sync_down($output);
			sync_down($output, true);
			$output = implode("\n", $output);
			echo $output . "\n";
			return strpos($output, 'up to date') !== false;	
		},
		'sync_up_empty_dir' => function() {
			setup();
			sync_up_callback($output, function() {
				mkdir('empty_dir');
			});
			$output = implode("\n", $output);
			echo $output . "\n";
			chdir('origin');
			return file_exists('empty_dir') && is_dir('empty_dir');
		},
		'sync_down_empty_dir' => function() {
			setup();
			sync_down_callback($output, function() {
				mkdir('empty_dir');
			});
			$output = implode("\n", $output);
			echo $output . "\n";
			chdir('local');
			return file_exists('empty_dir') && is_dir('empty_dir');
		},
		'sync_up_with_git_folder' => function() {
			setup();
			sync_up_callback($output, function() {
				exec('git init');
			});
			$output = implode("\n", $output);
			echo $output . "\n";
			chdir('origin');
			return test_cwd_is_git();
		},
		'sync_down_with_git_folder' => function() {
			setup();
			sync_down_callback($output, function() {
				exec('git init');
			});
			$output = implode("\n", $output);
			echo $output . "\n";
			chdir('local');
			return test_cwd_is_git();
		},
		'status_local' => function() {
			setup();
			sync_up($output); // test first with one commit
			$filename = "teststatuslocal-" . mt_rand();
			chdir('local');
			touch($filename);
			exec('php ../../git-sync status 2>&1', $output);
			$output = implode("\n", $output);
			return strpos($output, "L ?? $filename") !== false;
		},
		'status_remote' => function() {
			setup();
			sync_up($output); // test first with one commit
			$filename = "teststatuslocal-" . mt_rand();
			chdir('origin');
			touch($filename);
			chdir('../local');
			exec('php ../../git-sync status 2>&1', $output);
			$output = implode("\n", $output);
			return strpos($output, "R ?? $filename") !== false;
		},
		'status_both' => function() {
			setup();
			sync_up($output); // test first with one commit
			$filename = "teststatuslocal-" . mt_rand();
			chdir('origin');
			touch($filename);
			chdir('../local');
			touch($filename);
			exec('php ../../git-sync status 2>&1', $output);
			$output = implode("\n", $output);
			return strpos($output, "L ?? $filename") !== false && strpos($output, "R ?? $filename") !== false;;
		},
		'status_up_to_date' => function() {
			setup();
			sync_up($output); // test first with one commit
			chdir('local');
			exec('php ../../git-sync status 2>&1', $output);
			$output = implode("\n", $output);
			echo $output . "\n";
			return strpos($output, "local and remote up-to-date") !== false;
		},		
	);	
}

function clean_up() {
	global $cwd;
	chdir($cwd);
	exec('rm -rf repos/');
	mkdir('repos');	
}

function setup() {
	clean_up();
	chdir('repos');
	exec('git init origin');
	chdir('origin');
	exec('mv .git/ .sync');
	file_put_contents(".sync/info/exclude", ".sync*\n", FILE_APPEND);
	//exec('git --git-dir=.sync/ config --local sync.workdir ' . getcwd());
	//exec('cp ../../post-receive .sync/hooks/post-receive');
	exec('php ../../git-sync create-hook > .sync/hooks/post-receive');
	exec('chmod +x .sync/hooks/post-receive');
	chdir('..');
	exec('git clone origin/.sync local 2>&1');
	exec('mv local/.git/ local/.sync');
	file_put_contents("local/.sync/info/exclude", ".sync*\n", FILE_APPEND);
}

function sync_up(&$output, $skip_filechange = false) {
	chdir('local');
	if (!$skip_filechange) {
		file_put_contents('test.txt', "from local\n", FILE_APPEND);
	}
	exec('php ../../git-sync up 2>&1', $output);
	chdir('..');
}

function sync_down(&$output, $skip_filechange = false) {
	chdir('origin');
	if (!$skip_filechange) {
		file_put_contents('test.txt', "from origin\n", FILE_APPEND);
	}
	chdir('../local');
	exec('php ../../git-sync down 2>&1', $output);
	chdir('..');
}

function sync_up_callback(&$output, $callback) {
	chdir('local');
	$callback();
	exec('php ../../git-sync up 2>&1', $output);
	chdir('..');
}

function sync_down_callback(&$output, $callback) {
	chdir('origin');
	$callback();
	chdir('../local');
	exec('php ../../git-sync down 2>&1', $output);
	chdir('..');
}

function test_cwd_is_git() {
	$filename = "testgit-" . mt_rand();
	touch($filename);
	$result = exec('git status --p');
	unlink($filename);
	return strpos($result, "?? $filename") !== false;
}